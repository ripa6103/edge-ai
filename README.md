Supplemental Material for tinyML book https://tinymlbook.com/supplemental/
Source to some libraries https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro
Source to some examples https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples
Source to sparkfun tensorflow board info https://learn.sparkfun.com/tutorials/sparkfun-edge-hookup-guide?_ga=2.257043787.1581925614.1616406262-852650476.1616406262#hardware-overview
Source to sparkfun tutorials https://learn.sparkfun.com/tutorials
